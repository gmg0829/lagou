商家店铺表ods.ods_shops

```
DROP TABLE IF EXISTS `ods.ods_shops`; 

CREATE EXTERNAL TABLE `ods.ods_shops`( 
    shopId     int			,
    userId     int          ,
    areaId     int          ,
    shopName   string       ,
    shopLevel  tinyint      ,
    status     tinyint      ,
    createTime string       ,
    modifyTime string  
)
COMMENT '商家店铺表' 
PARTITIONED BY (`dt` string) 
row format delimited fields terminated by ',' 
location '/user/data/trade.db/shops/';
```

数据

```
-- 2020-09-20
100050,1,100225,WSxxx营超市,1,1,2020-09-20,2020-09-20 13:22:22
100052,2,100236,新鲜xxx旗舰店,1,1,2020-09-20,2020-09-20 13:22:22
100053,3,100011,华为xxx旗舰店,1,1,2020-09-20,2020-09-20 13:22:22

-- 2020-09-21
100052,22,100236,新鲜xxx旗舰店,1,1,2020-09-20,2020-09-21 13:22:22
100054,4,100159,小米xxx旗舰店,1,1,2020-09-21,2020-09-21 13:22:22
100055,5,100211,苹果xxx旗舰店,1,1,2020-09-21,2020-09-21 13:22:22

-- 2020-09-22
100053,35,100011,华为xxx旗舰店,1,1,2020-09-20,2020-09-22 13:22:22
100055,55,100211,苹果xxx旗舰店,1,1,2020-09-21,2020-09-22 13:22:22
100056,6,100050,OPxxx自营店,1,1,2020-09-22,2020-09-22 13:22:22
100057,7,100311,三只xxx鼠零食,1,1,2020-09-22,2020-09-22 13:22:22
100058,8,100329,良子xxx铺美食,1,1,2020-09-22,2020-09-22 13:22:22
```

商家店铺拉链表

```
drop table if exists dim.dim_shops; 
create table dim.dim_shops
(   `shopId`     int			,
    `userId`     int          ,
    `areaId`     int          ,
    `shopName`   string       ,
    `shopLevel`  tinyint      ,
    `status`     tinyint      ,
    `createTime` string       ,
    `modifyTime` string       ,
    `start_date`   string,
    `end_date`     string ) 
COMMENT '商家店铺拉链表' 
STORED AS PARQUET;
```



处理拉链表脚本

/work/lagou/data/shopszipper.sh

```shell
#!/bin/bash 
source /etc/profile 
if [ -n "$1" ] ;then
	do_date=$1 
else
	do_date=`date -d "-1 day" +%F` 
fi

sql=" 
insert overwrite table dim.dim_shops 
select shopId, userId, areaId, shopName, shopLevel, status, createTime, modifyTime , dt as start_date,                '9999-12-31' as end_date 
from ods.ods_shops 
where dt='$do_date'

union all 

select B.shopId, B.userId, B.areaId, B.shopName, B.shopLevel, B.status, B.createTime, B.modifyTime,                B.start_date,
       case when B.end_date='9999-12-31' and A.shopId is not null 
           then date_add('$do_date', -1) 
           else B.end_date 
       end as end_date 
 from (select * from ods.ods_shops where dt='$do_date') A 
     right join dim.dim_shops B 
     on A.shopId=B.shopId
distribute by shopId
"
hive -e "$sql"
```

问题

![image-20200926180224638](商家拉链表.assets/image-20200926180224638.png)

最后查询表为

```
shopid userid areaid shopname shoplevel	status createtime modifytime       start_date   end_date
100058	8	100329	良子xxx铺美食	1	1	2020-09-22	2020-09-22 13:22:22	2020-09-22	9999-12-31
100057	7	100311	三只xxx鼠零食	1	1	2020-09-22	2020-09-22 13:22:22	2020-09-22	9999-12-31
100056	6	100050	OPxxx自营店	 1	 1	 2020-09-22	 2020-09-22 13:22:22 2020-09-22	 9999-12-31
100055	55	100211	苹果xxx旗舰店	1	1	2020-09-21	2020-09-22 13:22:22	2020-09-22	9999-12-31
100053	35	100011	华为xxx旗舰店	1	1	2020-09-20	2020-09-22 13:22:22	2020-09-22	9999-12-31
100052	22	100236	新鲜xxx旗舰店	1	1	2020-09-20	2020-09-21 13:22:22	2020-09-21	9999-12-31
100054	4	100159	小米xxx旗舰店	1	1	2020-09-21	2020-09-21 13:22:22	2020-09-21	9999-12-31
100055	5	100211	苹果xxx旗舰店	1	1	2020-09-21	2020-09-21 13:22:22	2020-09-21	2020-09-21
100050	1	100225	WSxxx营超市	 1	 1	 2020-09-20	 2020-09-20 13:22:22 2020-09-20	 9999-12-31
100052	2	100236	新鲜xxx旗舰店	1	1	2020-09-20	2020-09-20 13:22:22	2020-09-20	2020-09-20
100053	3	100011	华为xxx旗舰店	1	1	2020-09-20	2020-09-20 13:22:22	2020-09-20	2020-09-21
Time taken: 0.146 seconds, Fetched: 11 row(s)

```

