package homework3

case class UserInfo(userName :String, location: String, startTime: Int, duration: Int)

object UserStatic {
  def main(args: Array[String]): Unit = {
    val userInfoList : List[UserInfo] = List(
      UserInfo("UserA","LocationA", 8, 60),
      UserInfo("UserA","LocationA", 9, 60),
      UserInfo("UserB","LocationB", 10, 60),
      UserInfo("UserB","LocationB", 11, 80)
    )

    val userMap: Map[String, List[UserInfo]] = userInfoList.groupBy(t => t.userName + "." + t.location)
    println(userMap)
    val orderByUserMap: Map[String, List[UserInfo]] = userMap.mapValues(t => t.sortBy(x => x.startTime))
    println(orderByUserMap)

    var firstTime : Int = 0
    val totalMap: Map[String, Int] = orderByUserMap.mapValues(t => {
      firstTime = t.head.startTime
      val sum = t.map(x => x.duration).sum
      sum
    })

    println(totalMap)
    totalMap.foreach{
      case (dates, sumTime) => println(s"$dates, $firstTime, $sumTime")
    }

  }

}
