package homework1



object Drink {

  // cap 瓶盖 ，empty 空瓶 bottle 啤酒瓶
  def buybeer(  cap:Int,empty: Int, bottle: Int): Int ={
    if ( empty < 3 && cap < 5) {
      return bottle
    }
    val empty_bottle = empty/3  // 空瓶可换的啤酒瓶数
    val empty_other = empty %3  // 空瓶换完剩余的空瓶
    val cap_bottle = cap / 5  // 瓶盖可换的啤酒瓶数
    val cap_other = cap % 5  // 瓶盖换完剩余的瓶盖数

    buybeer( empty_bottle + cap_bottle + cap_other, empty_bottle + cap_bottle + empty_other , empty_bottle + cap_bottle + bottle)
  }
  def main(args: Array[String]): Unit = {
    val bottle = 100 / 2
    val count = bottle + buybeer( bottle , bottle, 0)
    println(s"一共可以喝$count 瓶")
  }
}
