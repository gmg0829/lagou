package homework4

import akka.actor.{ActorRef, ActorSystem, Props}

object ActorGame {
  def main(args: Array[String]): Unit = {
    // 1 ActorSystem
    val actorFactor = ActorSystem("actorfactor")
    val bActorRef: ActorRef = actorFactor.actorOf(Props[BActor], "BActor")
    val aActorRef: ActorRef = actorFactor.actorOf(Props(new AActor(bActorRef)), "AActor")
    aActorRef ! "start"
  }
}