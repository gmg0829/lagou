package homework4

import akka.actor.{Actor, ActorRef}

class AActor(iBACtorRef: ActorRef) extends Actor{
  val bActorRef = iBACtorRef
  var count = 0
  override def receive: Receive = {
    case "start" => {
      println("AActor 启动")
      println("start ok")
      println("我打")
      //发给BActor
      bActorRef ! "我打"
    }
    case "我打" =>{
      count += 1
      println(s"AActor(黄飞鸿) 挺猛 看我佛山无影脚 第${count}脚")
      Thread.sleep(1000)
      bActorRef ! "我打"
    }
  }
}
