package homework2

import java.io.IOException

import scala.Char
import scala.util.Random


class User {
  val name: String = "游客"
  var score: Int = 0

  def showFist(box: String): String = {
    box match {
      case "1" => "你出拳:" + "剪刀"
      case "2" => "你出拳:" + "石头"
      case "3" => "你出拳:" + "布"
      case _ => "输入不符合规范，默认出布!"
    }
  }
}

class Computer(name: String) {
  var score: Int = 0

  def showFist(): Int = {
    val boxs: Array[String] = Array("剪刀", "石头", "布")
    println(name + "出拳！")
    val box = Random.nextInt(3)
    println(s"${name}出拳:${boxs(box)}")
    box + 1
  }
}

class Game(name: String) {
  val player = new User
  val playerCom = new Computer(name)
  var playTimes = 0
  var playerResult = scala.collection.mutable.Map("win" -> 0, "eq" -> 0, "lose" -> 0)
  var computerResult = scala.collection.mutable.Map("win" -> 0, "eq" -> 0, "lose" -> 0)

  def initGame(): Unit = {
    println(s"你选择了与${name}对战")
    println("要开始么?y/n")
    val begin = scala.io.StdIn.readLine()
    begin match {
      case "y" => playGame()
      case _ => println("退出游戏")
    }

  }

  def playGame(): Unit = {
    println("请出拳！1.剪刀 2.石头 3.布")
    var quit = false
    while (!quit) {
      var play_box = scala.io.StdIn.readLine()
      if (Set("1", "2", "3").contains(play_box)) {
        println(player.showFist(play_box))
      }
      else {
        player.showFist("err")
        play_box = "3"
      }
      val com_box = playerCom.showFist()

//      println(play_box, com_box)
      if(play_box.toInt == com_box){
        println("结果:和局!  下次继续努力!")
        player.score += 1
        playerCom.score += 1
        playerResult("eq") = playerResult("eq") + 1
        computerResult("eq") = computerResult("eq") + 1
        playTimes += 1

      }else if( (play_box.toInt - com_box.toInt == 1) || play_box.toInt - com_box.toInt == -2) // 赢
        {
          println("结果:恭喜, 你赢啦!")
          player.score += 2
          playerResult("win") = playerResult("win") + 1
          computerResult("lose") = computerResult("lose") + 1

          playTimes += 1
        }else{
        println("结果:很遗憾, 你输了! 下次努力")
        playerCom.score += 2
        playerResult("lose") = playerResult("lose") + 1
        computerResult("win") = computerResult("win") + 1
        playTimes += 1
      }

//      println("play:" + playerResult)
//      println("com:" + computerResult)
//      println("playTimes" + playTimes)

      println("是否开始下一轮(y/n)")
      quit = scala.io.StdIn.readLine() == "n"
      if(!quit){
        println("请出拳！1.剪刀 2.石头 3.布")
      }
    }
    println("退出游戏!")
    println("----------------------------------------------------------")
    println(s"$name  VS  游客")
    println(s"对战次数${playTimes}次")
    printf("%-8s\t%-8s\t%-8s\t%-8s\t%-8s\n","姓名","等分","胜局","和局","负局")
    printf("%-8s\t%-8s\t%-8s\t%-8s\t%-8s\n","游客",player.score.toString, playerResult("win").toString,playerResult("eq").toString,playerResult("lose").toString)
    printf("%-8s\t%-8s\t%-8s\t%-8s\t%-8s\n", name ,playerCom.score.toString, computerResult("win").toString,computerResult("eq").toString,computerResult("lose").toString)
    println("----------------------------------------------------------")
  }
}


object HumanComputer {
  def main(args: Array[String]): Unit = {
    //    val com = new Computer("刘备")
    //    val box = com.showFist()
    //    println(box)
    println("-----------------欢迎进入游戏世界-----------------")
    println("***********************************************")
    println("********************猜拳，开始*******************")
    println("***********************************************")
    println("请选择对战角色:(1.刘备  2.关羽  3.张飞)")
    try {
      val cname = scala.io.StdIn.readInt()
      cname match {
        case 1 => new Game("刘备").initGame()
        case 2 => new Game("关羽").initGame()
        case 3 => new Game("张飞").initGame()
      }
    } catch {
      case ex: Exception => {
        println("wrong input")
      }
    }
  }
}

