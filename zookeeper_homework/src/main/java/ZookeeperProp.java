import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ZookeeperProp {
    private static Connection conn = null;
    private static DataSource ds = null;
    public static void main(String[] args) throws IOException, SQLException, InterruptedException {
        // 获取zkClient对象
        final ZkClient zkClient = new ZkClient("node01:2181");
        //设置自定义的序列化类型,否则会报错！！
        zkClient.setZkSerializer(new ZkStrSerializer());


        //判断节点是否存在，不存在创建节点并赋值
        final boolean exists = zkClient.exists("/test/db");
        if (!exists) {
            zkClient.createPersistent("/test/db", true); //如果需要级联创建，第二个参数设置为true
//        System.out.println("path is created");

            Properties pro = new Properties();
            InputStream is = ZookeeperProp.class.getClassLoader().getResourceAsStream("druid.properties");
            pro.load(is);
            System.out.println(pro.toString());
            System.out.println(pro);
            //修改配置文件
            zkClient.writeData("/test/db",pro.toString().replaceAll("[{}]",""));
            //初始化 zookeeper 中的配置信息
        }
        JDBCUtils jdbcUtils = new JDBCUtils();
        ds = jdbcUtils.getDs();
        conn = jdbcUtils.getConnection();
        System.out.println("创建连接成功");
        String sql = "select *from test";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            System.out.println(resultSet.getString(1));
        }
        jdbcUtils.close(resultSet,preparedStatement,conn);
        ds = null;

        if(conn == null){
            System.out.println("连接关闭");
        }


        //注册监听器，节点数据改变的类型，接收通知后的处理逻辑定义
        zkClient.subscribeDataChanges("/test/db", new IZkDataListener() {
            public void handleDataChange(String path, Object data) throws Exception {
                //定义接收通知之后的处理逻辑
                System.out.println(path + " prop is changed  " );
                conn.close();
                ds = new JDBCUtils().getDs();
                conn = ds.getConnection();
                System.out.println("创建连接成功");
                String sql = "select *from test";
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    System.out.println(resultSet.getString(1));
                }
                conn.close();
                ds = null;
                if(conn == null){
                    System.out.println("连接关闭");
                }
            }


            //数据删除--》节点删除
            public void handleDataDeleted(String path) throws Exception {
                System.out.println(path + " is deleted!!");
            }
        });
        //删除节点
//        zkClient.delete("/lg-client1");
        Thread.sleep(Integer.MAX_VALUE);
    }
}
