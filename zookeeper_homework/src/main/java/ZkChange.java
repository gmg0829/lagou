import org.I0Itec.zkclient.ZkClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ZkChange {
    public static void main(String[] args) throws IOException {
        //先获取到zkclient对象,client与zk集群通信端口是2181
        final ZkClient zkClient = new ZkClient("node01:2181"); //建立了到zk集群的会话
        System.out.println("zkclient is ready");
        //1 创建节点
//        zkClient.createPersistent("/test/db", true); //如果需要级联创建，第二个参数设置为true
//        System.out.println("path is created");

        Properties pro = new Properties();
        InputStream is = ZkChange.class.getClassLoader().getResourceAsStream("druid.properties");
        pro.load(is);
        System.out.println(pro.toString());
        System.out.println(pro);
        boolean exists = zkClient.exists("/test.db");
        if(exists){
            System.out.println("/test/db存在");
        }
        //修改配置文件
        zkClient.writeData("/test/db",pro.toString().replaceAll("[{}]",""));
        String zkP =zkClient.readData("/test/db");

        //2删除节点
//        zkClient.delete("/la-client");
//        zkClient.deleteRecursive("/la-client");//递归删除可以删除非空节点，先删除子节点然后删除父节点
//        System.out.println("delete path is success");
//        zkClient.
    }
}
