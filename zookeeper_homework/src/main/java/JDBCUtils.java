import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.I0Itec.zkclient.ZkClient;

import javax.sql.DataSource;
import javax.swing.text.Style;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Arrays;
import java.util.Properties;

public class JDBCUtils {
    private static DataSource ds = null;
    //
    //获取连接池对象
    public  DataSource getDs(){
        try{
            //1.加载zookeeper配置文件
            //先获取到zkclient对象,client与zk集群通信端口是2181
            final ZkClient zkClient = new ZkClient("node01:2181"); //建立了到zk集群的会话
            System.out.println("zkclient is ready");

            Properties pro = new Properties();
//            InputStream is = JDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");
//            pro.load(is);
            String zkP =zkClient.readData("/test/db");

            String[] split = zkP.split(",");
            for (String str : split){
                String name = str.trim().split("=")[0];
                String value = str.trim().split("=")[1];
                pro.setProperty(name,value);
            }
            System.out.println(zkP);
//        System.out.println(zkP.replaceAll("[{}]",""));
            System.out.println("======================pro");
            System.out.println(pro);

            //2 初始化连接池
            ds = DruidDataSourceFactory.createDataSource(pro);
        }catch (IOException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        return ds;
    }

    // 获取连接对象
    public static Connection getConnection() throws SQLException{
        return ds.getConnection();
    }

    //释放连接方法
    public  void close(ResultSet rs, Statement stmt, Connection conn){
        if (rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (stmt != null){
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (conn != null){
            try {
                conn.close();//归还连接
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(ds != null){
            ds = null;
        }
    }

    public static void main(String[] args) throws SQLException {
        JDBCUtils jdbcUtils = new JDBCUtils();
        jdbcUtils.getDs();
        Connection conn = jdbcUtils.getConnection();
        System.out.println("创建连接成功");
        String sql = "select *from test";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            System.out.println(resultSet.getString(1));
        }
        jdbcUtils.close(resultSet,preparedStatement,conn);

        if(conn == null){
            System.out.println("连接关闭");
        }
    }
}
