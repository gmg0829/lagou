import com.sun.tools.classfile.ConstantPool;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class testProp {
    public static void main(String[] args) throws IOException {
        String test = "maxActive=10, password=root, url=jdbc:mysql://node01:3306/test, driverClassName=com.mysql.jdbc.Driver, initialSize=5, username=root";
        Properties pro = new Properties();
        String[] split = test.split(",");
        for (String str : split){
            String name = str.trim().split("=")[0];
            String value = str.trim().split("=")[1];
            pro.setProperty(name,value);
        }
        System.out.println(pro);
        System.out.println(pro.getProperty("url"));
    }

}
