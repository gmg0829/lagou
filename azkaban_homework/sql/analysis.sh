#!/bin/bash
source /etc/profile

day=`date +%Y%m%d`
echo $day

hive -e "insert into table user_info select id , substring(click_time,0,10) as \`date\` 
         from user_clicks 
         where dt='$day'  
         group by id, substring(click_time,0,10); "
