import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

public class TestJedisCluster {
    public static void main(String[] args) {
        JedisPoolConfig config = new JedisPoolConfig();
        Set<HostAndPort> jedisClusterNode = new HashSet<HostAndPort>();
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7001));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7002));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7003));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7004));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7005));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7006));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7007));
        jedisClusterNode.add(new HostAndPort("192.168.56.101", 7008));
        JedisCluster jcd = new JedisCluster(jedisClusterNode, config);
        jcd.set("name:002","zhangfei");
        String value = jcd.get("name:002");
        System.out.println(value);
    }
}
