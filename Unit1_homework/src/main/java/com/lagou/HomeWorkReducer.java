package com.lagou;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class HomeWorkReducer extends Reducer<IntWritable, NullWritable,IntWritable, IntWritable> {
    int id = 1;
    IntWritable k = new IntWritable();
    @Override
    protected void reduce(IntWritable key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        for(NullWritable value : values){
            k.set(id);
            context.write(k,key);
            id += 1;
        }
    }
}
