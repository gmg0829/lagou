package com.lagou;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class HomeWorkMapper extends Mapper<LongWritable, Text, IntWritable, NullWritable> {
    IntWritable k = new IntWritable();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 1 获取一行
        String line = value.toString();

        // 2 切割
        k.set(Integer.parseInt(line));
        // 3 输出
        context.write(k, NullWritable.get());
    }
}
